--
-- connect au serveur
-- ecoute /blub/#
-- envoi de temps en temps avec /blub/wemos
--
-- Version 3
--

local mymqtt={}


local ip="192.168.8.199"
local m=mqtt.Client("id"..node.chipid(),120,"21liAhIskp31GoWPvb5b")
local connected=false
-- tableau des messages a envoyer
local tosend={}

local subscribed={}

print("mqtt host="..ip.." qos=1")

-- fonction externe visible de partout
function publish(feed,value)
    --print("publication (connected="..tostring(connected)..") "..feed.." = "..tostring(value))
    -- qos = 1
    if not connected or not m:publish(feed,tostring(value),1,0) then
        print("Unable to publish. reconnect dans 10s");
        -- save for later
        tosend[feed]=tostring(value)
		local horloge=tmr.create()
        horloge:alarm(10 * 1000, tmr.ALARM_SINGLE, mymqtt.mqttInit )
    end
end

function subscribe(topic,f)
	m:subscribe(topic,0, function(client) print("subscribe success") end)
	subscribed[topic]=f
end
function unsubscribe(topic)
	subscribed[topic]=nil
end

-- fonction externe a remplacer
function receive(client,topic,data)
	if subscribed[topic] then
		subscribed[topic](data)
	else
    	print("recu: "..topic.." : "..data)
	end
end


function mymqtt.mqttInit()
    print("mqtt connecting to "..ip)
    m:close()
    connected=false;
    m:on("message", receive)
    m:connect(ip,1883,0,0,
	  function(client)
		print("mqtt connecte a "..ip)
        connected=true;
		--m:subscribe("blobule/f/#",0, function(client) print("subscribe success") end)
        -- empty the list of messages to send...
        for f,v in pairs(tosend) do publish(f,v) end
        tosend={}
	  end,
	  function(client, reason)
          print("failed reason: "..reason)
          print("retrying in 10sec")
		  local horloge=tmr.create()
          horloge:alarm(10 * 1000, tmr.ALARM_SINGLE, mymqtt.mqttInit )
      end
    )
end

--function mymqtt.mqttInitHost(host)
--    net.dns.resolve(host,function(sk,ip) print("got ip="..ip) mymqtt.mqttInit(ip) end)
--end

mymqtt.m=m  -- pour access exterieur
mymqtt.mqttInit()



return mymqtt



