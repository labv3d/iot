--
-- connect au serveur
-- ecoute /blub/#
-- envoi de temps en temps avec /blub/wemos
--

local mymqtt={}


local serveur="192.168.8.199"

local m=mqtt.Client("id"..node.chipid(),120,"21liAhIskp31GoWPvb5b")
local horloge=tmr.create()
local connected=false
-- tableau des messages a envoyer
local tosend={}

print("!mqtt host="..serveur)

-- fonction externe visible de partout
function publish(feed,value,qos)
    --print("publication (connected="..tostring(connected)..") "..feed.." = "..tostring(value))
    -- qos = 1 par defaut
    if not connected or not m:publish(feed,tostring(value),qos or 1,0) then
        print("Unable to publish. reconnect dans 10s");
        -- save for later
        tosend[feed]=tostring(value)
        horloge:alarm(10 * 1000, tmr.ALARM_SINGLE, mymqtt.mqttInit )
    end
end

--
-- actions["lampe/couleur"]=function (topic,message) .... end
-- execute la fonction pour un topic
--
local actions={}

-- fonction externe a remplacer
function receive(client,topic,data)
	if type(actions[topic])=="function" then actions[topic](topic,data) else
		print("recu: "..topic.." : "..data)
	end
end

function subscribe(topic,f,qos)
	if f then
		m:subscribe(topic,qos or 0, function(client) print("subscribe success!") end)
	else
		m:unsubscribe(topic, function(client) print("unsubscribe success!") end)
	end
	actions[topic]=f
end

function mymqtt.mqttInit()
    print("mqtt connecting to "..serveur)
    m:close()
    connected=false;
    m:on("message", receive)
    m:connect(serveur,1883,0,0,
	  function(client)
		print("mqtt connecte a "..serveur)
        connected=true;
		--m:subscribe("blobule/#",0, function(client) print("subscribe success") end)
        -- empty the list of messages to send...
        for f,v in pairs(tosend) do publish(f,v) end
        tosend={}
	  end,
	  function(client, reason)
          print("failed reason: "..reason)
          print("retrying in 10sec")
          horloge:alarm(10 * 1000, tmr.ALARM_SINGLE, mymqtt.mqttInit )
      end
    )
end

--function mymqtt.mqttInitHost(host)
--    net.dns.resolve(host,function(sk,ip) print("got ip="..ip) mymqtt.mqttInit(ip) end)
--end

mymqtt.m=m  -- pour access exterieur
mymqtt.mqttInit()



return mymqtt



