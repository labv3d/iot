
--
-- LED matrix module
--


local dataPin=7
local clockPin=5


-- inverse ordre des bits
local function flip(v)
	local w=0x00
	for i=0,7 do
		if bit.isset(v,i) then w=bit.set(w,7-i) end
	end
	return w
end

local function stop()
	--tr:stop()
	gpio.write(7,0); gpio.write(5,1); gpio.write(7,1); gpio.write(5,0)
end

local function start()
	gpio.write(7,1); gpio.write(5,1); gpio.write(7,0); gpio.write(5,0)
end

local function send(data)
    for i=0,7 do
        gpio.write(7,bit.isset(data,i) and 1 or 0)
        gpio.write(5,1)
        gpio.write(5,0)
    end
end

local buffer={}
local long=0
-- nb de ints dans le buffer.
-- nb=nb de lettres/3
--nb=3

--
-- AABBCCDD DDEEFGG -> int32
-- pour afficher: 
-- 00081624 00081624
-- on attend des shift de 0 a 24 (pour 3 char) * nb
local function affiche(s)
		local nb=#(buffer[1])
		s=s%(long*8)
		b=math.floor(s/24)%nb
		s=24-(s%24)
		start();send(0x40);stop()
		start();send(0xC0)
		for i=1,8 do
			send(bit.band(bit.rshift(buffer[i][b+1],s),0x000000ff))
		end
		stop()
end

-- timer pour le texte
local tr=tmr.create()

function texte(n)
	intensity(7)
	local shift=0
	tr:register(n,tmr.ALARM_AUTO,function()
		affiche(shift)
		shift=(shift+1)
	end)
	tr:start()
end


function animation(n)
	intensity(7)
	local shift=0
	tr:register(n,tmr.ALARM_AUTO,function()
		affiche(shift)
		shift=(shift+8)
	end)
	tr:start()
end

function off(t)
	start();send(0x80);stop()
end

function intensity(t)
	-- 0 a 7
	start();send(bit.bor(0x88,t or 4));stop()
end


function convert(s)
	local v={}
	local j=1
    for i=1,s:len(),2 do
        v[j]=string.char(flip(tonumber(s:sub(i,i+1),16)))
		j=j+1
    end
	return table.concat(v)
end

local lettres={
[' ']="0000000000000000",
['0']="3E63737B6F673E00",
['1']="0C0E0C0C0C0C3F00",
['2']="1E33301C06333F00",
['3']="1E33301C30331E00",
['4']="383C36337F307800",
['5']="3F031F3030331E00",
['6']="1C06031F33331E00",
['7']="3F3330180C0C0C00",
['8']="1E33331E33331E00",
['9']="1E33333E30180E00",
['.']="00000000000C0C00",
['A']="0C1E33333F333300",
['B']="3F66663E66663F00",
['C']="3C66030303663C00",
['D']="1F36666666361F00",
['E']="7F46161E16467F00",
['F']="7F46161E16060F00",
['G']="3C66030373667C00",
['H']="3333333F33333300",
['I']="1E0C0C0C0C0C1E00",
['J']="7830303033331E00",
['K']="6766361E36666700",
['L']="0F06060646667F00",
['M']="63777F7F6B636300",
['N']="63676F7B73636300",
['O']="1C36636363361C00",
['P']="3F66663E06060F00",
['Q']="1E3333333B1E3800",
['R']="3F66663E36666700",
['S']="1E33070E38331E00",
['T']="3F2D0C0C0C0C1E00",
['U']="3333333333333F00",
['V']="33333333331E0C00",
['W']="6363636B7F776300",
['X']="6363361C1C366300",
['Y']="3333331E0C0C1E00",
['Z']="7F6331184C667F00",
['?']="1E3330180C000C00",
['!']="183C3C1818001800",
['*']="3C42A581A599423C",
['@']="3C42A5A581BD423C"
--[[
['!']="183C3C1818001800",
['"']="3636000000000000",
['#']="36367F367F363600",
['$']="0C3E031E301F0C00",
['%']="006333180C666300",
['&']="1C361C6E3B336E00",
["'"]="0606030000000000",
['(']="180C0606060C1800",
[')']="060C1818180C0600",
['*']="00663CFF3C660000",
['+']="000C0C3F0C0C0000",
[',']="00000000000C0C06",
['-']="0000003F00000000",
['.']="00000000000C0C00",
['/']="6030180C06030100",
['0']="3E63737B6F673E00",
['1']="0C0E0C0C0C0C3F00",
['2']="1E33301C06333F00",
['3']="1E33301C30331E00",
['4']="383C36337F307800",
['5']="3F031F3030331E00",
['6']="1C06031F33331E00",
['7']="3F3330180C0C0C00",
['8']="1E33331E33331E00",
['9']="1E33333E30180E00",
[':']="000C0C00000C0C00",
[';']="000C0C00000C0C06",
['<']="180C0603060C1800",
[']']="00003F00003F0000",
['>']="060C1830180C0600",
['?']="1E3330180C000C00",
['@']="3E637B7B7B031E00",
['A']="0C1E33333F333300",
['B']="3F66663E66663F00",
['C']="3C66030303663C00",
['D']="1F36666666361F00",
['E']="7F46161E16467F00",
['F']="7F46161E16060F00",
['G']="3C66030373667C00",
['H']="3333333F33333300",
['I']="1E0C0C0C0C0C1E00",
['J']="7830303033331E00",
['K']="6766361E36666700",
['L']="0F06060646667F00",
['M']="63777F7F6B636300",
['N']="63676F7B73636300",
['O']="1C36636363361C00",
['P']="3F66663E06060F00",
['Q']="1E3333333B1E3800",
['R']="3F66663E36666700",
['S']="1E33070E38331E00",
['T']="3F2D0C0C0C0C1E00",
['U']="3333333333333F00",
['V']="33333333331E0C00",
['W']="6363636B7F776300",
['X']="6363361C1C366300",
['Y']="3333331E0C0C1E00",
['Z']="7F6331184C667F00",
['[']="1E06060606061E00",
['\\']="03060C1830604000",
[']']="1E18181818181E00",
['^']="081C366300000000",
['_']="00000000000000FF",
['`']="0C0C180000000000",
['a']="00001E303E336E00",
['b']="0706063E66663B00",
['c']="00001E3303331E00",
['d']="3830303e33336E00",
['e']="00001E333f031E00",
['f']="1C36060f06060F00",
['g']="00006E33333E301F",
['h']="0706366E66666700",
['i']="0C000E0C0C0C1E00",
['j']="300030303033331E",
['k']="070666361E366700",
['l']="0E0C0C0C0C0C1E00",
['m']="0000337F7F6B6300",
['n']="00001F3333333300",
['o']="00001E3333331E00",
['p']="00003B66663E060F",
['q']="00006E33333E3078",
['r']="00003B6E66060F00",
['s']="00003E031E301F00",
['t']="080C3E0C0C2C1800",
['u']="0000333333336E00",
['v']="00003333331E0C00",
['w']="0000636B7F7F3600",
['x']="000063361C366300",
['y']="00003333333E301F",
['z']="00003F190C263F00",
['{']="380C0C070C0C3800",
['|']="1818180018181800",
['}']="070C0C380C0C0700",
['~']="6E3B000000000000"
--]]
}


function message(msg)
	long=msg:len()
	bufffer={}
	for row=1,8 do
		buffer[row]={}
		for i=0,long-1,3 do
			num=0
			for j=0,3 do
				local k=(i+j)%long+1
				local c=msg:sub(k,k)
				local by=lettres[c]:byte(row) -- 1 a 8 pour les rangess
				num=bit.bor(num,bit.lshift(by,24-j*8))
			end
			buffer[row][i/3+1]=num
			--print(row,i,num)
		end
	end
end

function bitmap(key,value)
	lettres[key]=convert(value)
end

print("-- matrix led module --")

gpio.mode(dataPin,gpio.OUTPUT)
gpio.mode(clockPin,gpio.OUTPUT)
stop()

-- conversion hexa string en binaire
for k,v in pairs(lettres) do
	lettres[k]=convert(v)
end

message(" SEJOUR 2020 *")
texte(70)
--animation(70)

local m={}
m.message=message
m.texte=texte
m.intensity=intensity
m.bitmap=bitmap
return m

--
-- pour mqtt
-- actions["blobule/m"]=function (t,d) message(d) end
--
