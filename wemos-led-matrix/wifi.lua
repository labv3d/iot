--
-- pour connecter au reseau sejour2019 avec le mot de passe bonjour!
--
-- wifi("sejour2019","bonjour!")
--
-- Si on veut la liste des reseaux disponibles:
--
-- wifi()
--
-- NOTE: On ne fait wifi("x","y") qu'une fois. Apres c'est automatique
--

local ssid,password=...

if not ssid then
	-- pas de ssid? on liste les reseaux visibles
	print("reseaux wifi visibles ...")
	local function listap(t) for k,v in pairs(t) do print(k) end end
    wifi.sta.getap(listap)
else
	wifi.setmode(wifi.STATION)

	config={}
	config.ssid=ssid
	config.pwd=password
	print("Connexion au wifi... ("..config.ssid..")")
	wifi.sta.config(config)

	local function tryIp(t)
		print(".")
		ip = wifi.sta.getip()
		if ( ( ip ~= nil ) and  ( ip ~= "0.0.0.0" ) )then
			print("Succes! notre ip : ",ip)
			t:unregister()
		end
	end

	-- attendre une adresse ip
	tmr.create():alarm( 1000, tmr.ALARM_AUTO, tryIp )
end

